#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include "HeaderForTasks1,3,4.h"


void GetLane(char *lane)
{
	puts("Enter your line:");
	fgets(lane, SIZELEN, stdin);
	lane[strlen(lane) - 1] = 0;
}
void CreatePointers(char *lane)
{
	char *pointers[NUMPOINTERS];
	int i = 0;
	int j = 0;
	for (i; i < strlen(lane); i++)
	{
		if (isalnum(lane[i]) && lane[i - 1] == ' ' || isalnum(lane[i]) && i == 0 )
		{
			pointers[j] = lane + i;
			j++;
		}
	}
	ShakeNum(j - 1, pointers);
}
void ShakeNum(int numwords, char **pointers)
{
	char *bufPointer;
	int i;
	int randword1, randword2;
	for (i = numwords; i >= 0;i--)
	{
		randword1 = rand() % numwords;
		randword2 = rand() % numwords + 1;
		bufPointer = pointers[randword1];
		pointers[randword1] = pointers[randword2];
		pointers[randword2] = bufPointer;
	}
	for (i = 0; i <= numwords; i++)
	{
		bufPointer = pointers[i];
		while (isalnum(*bufPointer))
		{
			putchar(*bufPointer);
			bufPointer++;
		}
		putchar(' ');
	}

}
void ShakeWords(char *line)
{
	char word[SIZELEN];
	int i = 0, j = 0;
	int count, lenword;
	int randnum1, randnum2;
	char bufnum;
	for(i; i < strlen(line); i++)
	{
		if (isalnum(line[i]))
		{	
			while (isalnum(line[i]))
			{
				word[j] = line[i];
				i++;
				j++;
			}
			word[j] = 0;
			count = j;
			lenword = j - 2;
			if (lenword > 0)
			{
				while (count)
				{
					randnum1 = rand() % lenword + 1 ;
					randnum2 = rand() % lenword + 1;
					bufnum = word[randnum1];
					word[randnum1] = word[randnum2];
					word[randnum2] = bufnum;
					count--;

				}
				printf("%s ",word);
				
			}
			else
			{
				printf("%s ", word);
				
			}
			j = 0;
		}
	}
	putchar('\n');
}
