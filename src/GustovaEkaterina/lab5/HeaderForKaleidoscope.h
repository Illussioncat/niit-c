#define HIGHT 30
#define LEN 60
#define NumSym 35
#define SEC 1000

void CleanArr(char(*arr)[LEN]);
void FormationQuarter(char(*arr)[LEN]);
void FormationArr(int i, int j, char(*arr)[LEN]);
void PrintArr(char(*arr)[LEN]);