#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Header.h"

SYM *makeTree(SYM *psym[], int N)
{
	SYM * temp = (SYM*)malloc(sizeof(SYM));
	temp->freq = psym[N - 1]->freq + psym[N - 2]->freq;
	temp->code[0] = 0;
	temp->left = psym[N - 1];
	temp->right = psym[N - 2];

	if (N == 2)
		return temp;
	else
	{
		for (int i = 0;  i < N ; i++)
			if (temp->freq>psym[i]->freq)
			{
				for (int j = N - 1; j > i; j--)
					psym[j] = psym[j - 1];

				psym[i] = temp;
				break;
			}
	}
	return makeTree(psym, N - 1);
}

void makeCodes(SYM *root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

